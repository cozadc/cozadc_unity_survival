﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpManager : MonoBehaviour {

	public static int jumpScore;
	Text text;


	void Awake()
	{
		text = GetComponent <Text> ();
		jumpScore = 0;
	}


	// Update is called once per frame
	void Update () {
		text.text = "Jumps: " + jumpScore;
	}
}
