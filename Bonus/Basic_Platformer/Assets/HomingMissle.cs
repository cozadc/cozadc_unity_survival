﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
public class HomingMissle : MonoBehaviour {

	public Transform target;

	public float speed =10f;
	public float rotateSpeed=200f;

	public GameObject explosionEffect;

	private Rigidbody2D rb;
	//public GameObject ExplosionEffect;
	void increaseSpeed()
	{
		speed = speed + .6f;
		if (speed > 20)
			speed = 20;
	}
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
			InvokeRepeating ("increaseSpeed", 2f, 4f);
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
		Vector2 direction = (Vector2)target.position - rb.position; 
		direction.Normalize();
		float rotateAmount = Vector3.Cross(direction, transform.up).z;
		rb.angularVelocity = -rotateAmount * rotateSpeed;
		rb.velocity = transform.up * speed;﻿
	}﻿
	void OnTriggerEnter2D (Collider2D other) {
		//Instantiate (ExplosionEffect, transform.position, transform.rotation);
		if (other.gameObject.CompareTag ("Player"))
			


			Destroy (gameObject);
			Instantiate (explosionEffect, transform.position, transform.rotation);
			speed = 10;
	}
}
