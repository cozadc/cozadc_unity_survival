﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	public Transform Player;
	public Vector3 offset;

	void Update ()
	{
		transform.position = new Vector3 (Player.position.x + offset.x, Player.position.y + offset.y, Player.position.z+ offset.z);
	}
}